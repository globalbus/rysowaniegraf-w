﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace RysowanieGrafów
{
    public class Graph
    {
        ICollection<Edge> edges;
        ICollection<Node> nodes;
        public ICollection<Edge> Edges { get { return (edges as List<Edge>).AsReadOnly(); } }
        public ICollection<Node> Nodes { get { return (nodes as List<Node>).AsReadOnly(); } }
        bool IsSplitted = false;
        Image generatedImage;
        public Image GeneratedImage { get { return generatedImage; }}
        Bitmap labeledImage;
        public Image LabeledImage { get { return labeledImage; } }
        /// <summary>
        /// Gets the node radius from settings keeper to short form of notation
        /// </summary>
        public int NodeRadius
        {
            get { return SettingsKeeper.Data.NodeRadius; }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Graph"/> class.
        /// </summary>
        /// <param name="file">The file in gph own format</param>
        public Graph(String file)
        {
            try
            {
                ReadFile(Encoding.GetEncoding("UTF-8").GetChars(File.ReadAllBytes(file)));//Fast raw reading
            }
            catch (FileNotFoundException e)
            {
                throw new Exception("File cannot be found", e);
            }
            catch (IOException e)
            {
                throw new Exception("Error while reading file to memory", e);
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Graph"/> class.
        /// </summary>
        /// <param name="p">data in gph own format</param>
        public Graph(byte[] p)
        {
            ReadFile(Encoding.GetEncoding("UTF-8").GetChars(p));
        }
        /// <summary>
        /// Reads the file.
        /// Proof to syntax error as it is possible. Not a stricly parser
        /// </summary>
        /// <param name="data">The data.</param>
        private void ReadFile(char[] data)
        {
            edges = new List<Edge>(); //construct list
            nodes = new List<Node>();
            int i=0;
            char[] table=null;
            try
            {
                table = data;
                int pointer = 0;//starting point
                bool startNodes = false;
                bool startEdges = false;
                bool startDecoding = false;
                int nodeId = -1;
                Node node1=null;
                Node node2=null;
                int relation=0;
                string nodeLabel;
                for (;i<table.Length; i++)
                {
                    if (table[i] == 'n' || table[i] == 'N')//start reading nodes
                        if (new string(table, i, 7).ToLower() == "nodes={")
                        {
                            i += 6;
                            startEdges = false;
                            startNodes = true;
                            startDecoding=false;
                            continue;
                        }
                   if (table[i] == 'e' || table[i] == 'E')//start reading edges
                            if (new string(table, i, 7).ToLower() == "edges={")
                            {
                                i += 6;
                                startNodes = false;
                                startEdges = true;
                                startDecoding=false;
                                continue;
                            }
                    if (table[i] == '#')//skip comments
                        while (table[i] != 10)
                            i++;
                    else if (startNodes)
                    {

                            if (startDecoding)
                            {
                                if (Char.IsWhiteSpace(table[i]))//ignore whitespaces
                                    continue;
                                else
                                    if (Char.IsDigit(table[i])&&nodeId==-1)//read integer
                                    {
                                        pointer = i;
                                        while (i < table.Length && Char.IsDigit(table[i]))
                                            i++;
                                        nodeId = Int32.Parse(new string(table, pointer, i - pointer));
                                        if (nodeId < 0)
                                            throw new Exception("NodeId must be greater than 0");
                                        if (nodes.Any((x) => (x.NodeId == nodeId)))
                                            throw new Exception("NodeId must be unique");
                                    }
                                    else if (!Char.IsWhiteSpace(table[i]))//read string
                                    {
                                        pointer = i;
                                        while (i < table.Length && table[i] != '}')
                                            i++;
                                        nodeLabel = new string(table, pointer, i - pointer);
                                        nodes.Add(new Node(nodeId, nodeLabel));
                                        startDecoding = false;
                                        nodeId = -1;
                                    }
                                continue;
                            }
                            else if (table[i] == '{')//start symbol for decode
                                startDecoding = true;
                    }
                    else if (startEdges)
                    {
                        if (startDecoding)
                        {
                            if (Char.IsWhiteSpace(table[i]))//ignore whitespaces
                                continue;
                            else
                                if (Char.IsDigit(table[i]))
                                {
                                    pointer = i;
                                    while (i < table.Length && Char.IsDigit(table[i]))
                                        i++;
                                    if (nodeId < 0)//read first integer
                                    {
                                        nodeId = Int32.Parse(new string(table, pointer, i - pointer));
                                        if (nodeId < 0)
                                            throw new Exception("NodeId must be greater than 0");
                                        if (!nodes.Any((x) => (x.NodeId == nodeId)))
                                            throw new Exception("NodeId must exists in node list");
                                        else
                                            node1 = nodes.First((x) => (x.NodeId == nodeId));
                                    }
                                    else
                                    {
                                        nodeId = Int32.Parse(new string(table, pointer, i - pointer));
                                        if (nodeId < 0)
                                            throw new Exception("NodeId must be greater than 0");
                                        if (!nodes.Any((x) => (x.NodeId == nodeId)))
                                            throw new Exception("NodeId must exists in node list");
                                        else
                                            node2 = nodes.First((x) => (x.NodeId == nodeId));
                                        Edge temp;
                                        if (relation == 0)
                                            temp = new Edge(node1, node2, true);
                                        else if (relation == 1)
                                            temp = new Edge(node1, node2, false);
                                        else
                                            temp = new Edge(node2, node1, false);
                                        edges.Add(temp);
                                        nodes.First((x) => (x == node1)).AddEdge(temp);
                                        if (node1 != node2)
                                            nodes.First((x) => (x == node2)).AddEdge(temp);
                                        node1 = null;
                                        node2 = null;
                                        nodeId = -1;
                                        startDecoding = false;
                                    }

                                }
                            if (table[i] == '<')
                                relation = -1;
                            else if (table[i] == '-')
                                relation = 0;
                            else if (table[i] == '>')
                                relation = 1;
                            else if (table[i] == '}')//end symbol for decode
                                startDecoding = false;
                        }
                        else if (table[i] == '{')//start symbol for decode
                            startDecoding = true;
                        continue;
                    }
                }
                GC.SuppressFinalize(table);//free memory
            }
            //throw all exceptions to GUI

            catch (Exception e)
            {
                //Decode line/column in file where error occurred
                int line=0;
                int j=0, k=0;
                if (i > 0 && table != null)
                {
                    for (; j < i; j++, k++)
                        if (table[j] == 10)
                        {
                            line++; k = 0;
                        }
                    throw new Exception(String.Format("Parse error near line {0}, column {1}", line,k), e);
                }
                else
                    throw e;
            }
        }

        /// <summary>
        /// Splits this instance.
        /// </summary>
        /// <returns>graphSet</returns>
        public ICollection<Graph> Split() //Split single graph into set of a connected graphs
        {
            IList<Node> temp = new List<Node>(nodes);
            IEqualityComparer<Edge> comparer = new EqComparer();
            IList<Graph> newGraphs = new List<Graph>();
            while (temp.Count > 0)
            {
                List<Node> newNodes = new List<Node>();
                List<Edge> newEdges = new List<Edge>();
                Node node = temp[0];
                newNodes.Add(node);
                int i = 0;
                while (i < newNodes.Count)
                {
                    node = newNodes[i];
                    temp.Remove(node);
                    if (!newNodes.Contains(node))
                        newNodes.Add(node);
                    foreach (var item in node.EdgesRelatedWithThisNode)
                    {
                        if (!newNodes.Contains(item.Node1))
                            newNodes.Add(item.Node1);
                        if (!newNodes.Contains(item.Node2))
                            newNodes.Add(item.Node2);
                        if (!newEdges.Contains(item, comparer))
                            newEdges.Add(item);
                    }
                    i++;
                }
                newGraphs.Add(new Graph(newNodes, newEdges));
            }
            return newGraphs;
        }
        /// <summary>
        /// Visualize set of graphs as single graph 
        /// </summary>
        /// <param name="graphSet">The graph set.</param>
        /// <returns>Bitmap of a graph set</returns>
        public static Bitmap ToBitmap(ICollection<Graph> graphSet)
        {
            if (graphSet.Count > 0)
            {
                IList<Bitmap> bitmapSet = new List<Bitmap>();
                Bitmap temp;
                foreach (var item in graphSet)
                {
                    temp = item.ToBitmap();
                    if (SettingsKeeper.Data.IsLabeled) temp = item.DoLabels();
                    int size = temp.Height;
                    int i = 0;
                    int s = bitmapSet.Count;
                    while (i < s && size > (bitmapSet[i].Height))
                    {
                        i++;
                    }
                    bitmapSet.Insert(i, temp);
                }
                if (bitmapSet.Count == 1) return bitmapSet[0];
                Bitmap concatedBitmap=bitmapSet[0];
                for (int i = 1; i < bitmapSet.Count; i++)
                {
                    if (concatedBitmap.Width > concatedBitmap.Height)
                    {
                        temp = new Bitmap(Math.Max(concatedBitmap.Width, bitmapSet[i].Width), concatedBitmap.Height + bitmapSet[i].Height);
                        Graphics g = Graphics.FromImage(temp);
                        g.DrawImage(bitmapSet[i], 0, 0);
                        g.DrawImage(concatedBitmap, 0, bitmapSet[i].Height);
                    }
                    else
                    {
                        temp = new Bitmap(concatedBitmap.Width + bitmapSet[i].Width, Math.Max(concatedBitmap.Height, bitmapSet[i].Height));
                        Graphics g = Graphics.FromImage(temp);
                        g.DrawImage(bitmapSet[i], 0, 0);
                        g.DrawImage(concatedBitmap, bitmapSet[i].Width, 0);
                    }
                    concatedBitmap = temp;
                }
                return concatedBitmap;
            }
            return null;
        }
        /// <summary>
        /// Visualize single graph in circle layout or as tree if it's possible.
        /// </summary>
        /// <returns>Bitmap</returns>
        int loopSpacing;
        private Bitmap ToBitmap()
        {
            int numberOfNodes = nodes.Count;
            loopSpacing = NodeRadius;//to check?
            int graphSize = (int)(NodeRadius * Math.Pow(3,1.0/numberOfNodes)* numberOfNodes + 2*loopSpacing); //Formula to test - near possible optimum value;

            Bitmap nodesImage = new Bitmap(graphSize, graphSize);
            Bitmap loopImage = new Bitmap(graphSize+loopSpacing*2, graphSize+loopSpacing*2);//external image
            Bitmap edgesImage = new Bitmap(graphSize-4*NodeRadius, graphSize-4*NodeRadius);//internal image

            Brush nodeBrush = new SolidBrush(SettingsKeeper.Data.NodeColor);
            Pen edgePen = new Pen(SettingsKeeper.Data.EdgeColor, SettingsKeeper.Data.EdgeThickness);
            Pen edgeDirectionalPen = new Pen(SettingsKeeper.Data.EdgeColor, SettingsKeeper.Data.EdgeThickness);
            edgeDirectionalPen.EndCap = LineCap.Custom;
            GraphicsPath CustomPenCapPath = new GraphicsPath();
            CustomPenCapPath.AddLine(new Point(0, 0), new Point(-2, -5));
            CustomPenCapPath.AddLine(new Point(0, 0), new Point(2, -5));
            edgeDirectionalPen.CustomEndCap = new CustomLineCap(null, CustomPenCapPath, LineCap.ArrowAnchor,0.0f);

            int center = graphSize / 2;
            int x = center;
            int y = NodeRadius;
            double angle = 2 * Math.PI / numberOfNodes;
            using (Graphics g = Graphics.FromImage(nodesImage))
            {
                g.SmoothingMode = SettingsKeeper.Data.SmoothingMode;
                //helper for debugging
                //g.DrawEllipse(edgePen, 0, 0, graphSize, graphSize);
                int i = 0;
                int tempx;
                int tempy;
                foreach (var node in nodes)
                {
                    node.X = x-NodeRadius;
                    node.Y = y-NodeRadius;
                    node.Angle = i * angle; //for edges drawing
                    g.FillEllipse(nodeBrush, x - NodeRadius, y - NodeRadius, 2 * NodeRadius, 2 * NodeRadius);
                    //rotation around center of a circle
                    tempx = (int)((x - center) * Math.Cos(angle) - (y - center) * Math.Sin(angle) + center);
                    tempy = (int)((x - center) * Math.Sin(angle) + (y - center) * Math.Cos(angle) + center);
                    x = tempx;
                    y = tempy;
                    i++;
                }
            }
            Pen actual;
            //drawing internal edges
            using (Graphics g = Graphics.FromImage(edgesImage))
            {
                g.SmoothingMode = SettingsKeeper.Data.SmoothingMode;
                foreach (var edges in GroupEdges())
                {
                    Edge positioningEdge = edges.First();
                    double scale = 0.5;
                    int i = 1;
                    //calculation of a shift vectors
                    int movex2 = (int)(NodeRadius * Math.Sin((positioningEdge.Node2.Angle)));
                    int movey2 = (int)(-NodeRadius * Math.Cos((positioningEdge.Node2.Angle)));
                    int movex1 = (int)(NodeRadius * Math.Sin((positioningEdge.Node1.Angle)));
                    int movey1 = (int)(-NodeRadius * Math.Cos((positioningEdge.Node1.Angle)));
                    Point start = new Point(positioningEdge.Node1.X - loopSpacing - movex1, positioningEdge.Node1.Y - loopSpacing - movey1);
                    Point end = new Point(positioningEdge.Node2.X - loopSpacing - movex2, positioningEdge.Node2.Y - loopSpacing - movey2);
                    int centerx = (start.X + end.X) / 2;
                    int centery = (start.Y + end.Y) / 2;
                    foreach (var edge in edges)
                    {
                        if (!edge.IsLoopback)
                        {
                            
                            if (i == edges.Count())
                                scale = 0.0;
                            if (edge.IsBidirectional)
                                actual = edgePen;
                            else
                                actual = edgeDirectionalPen;
                            
                            angle = Math.PI / 2 * (2 * (i % 2) - 1);
                            int aberrancex = (int)((start.X - centerx) * Math.Cos(angle) - (start.Y - centery) * Math.Sin(angle) + centerx);
                            int aberrancey = (int)((start.X - centerx) * Math.Sin(angle) + (start.Y - centery) * Math.Cos(angle) + centery);

                            Point middle = new Point((int)(centerx * (1 - scale) + (aberrancex * scale)), (int)(centery * (1 - scale) + (aberrancey * scale)));
                            Point[] array = new Point[3] { start, middle, end };
                            g.DrawCurve(actual, array);
                            if(i%2==0)
                                scale -= scale / edges.Count *2;//formula to check
                            i++;
                        }
                    }
                }
            }
            //drawing loops
            using (Graphics g = Graphics.FromImage(loopImage))
            {
                g.SmoothingMode = SettingsKeeper.Data.SmoothingMode;
                foreach (var edge in edges)
                {
                    if (edge.IsLoopback)//loop drawing
                    {
                        int movex = (int)(loopSpacing * Math.Sin(edge.Node1.Angle));
                        int movey = (int)(-loopSpacing * Math.Cos(edge.Node1.Angle));
                        g.DrawEllipse(edgePen, edge.Node1.X + movex + loopSpacing + NodeRadius/2, edge.Node1.Y + movey + loopSpacing + NodeRadius/2, NodeRadius, NodeRadius);//to calculate - size of a loop
                    }
                }
            }
            //merging phase
            using (Graphics g = Graphics.FromImage(loopImage))
            {
                g.DrawImage(nodesImage, new Point(loopSpacing, loopSpacing));
                g.DrawImage(edgesImage, new Point(loopSpacing+2*NodeRadius, loopSpacing+2*NodeRadius));
            }
            //cleanup
            nodesImage.Dispose();
            edgesImage.Dispose();
            CustomPenCapPath.Dispose();
            edgeDirectionalPen.Dispose();
            edgePen.Dispose();
            nodeBrush.Dispose();
            generatedImage = loopImage;
            return loopImage;
        }
        /// <summary>
        /// Constructor for generate single splitted graph
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="edges">The edges.</param>
        private Graph(ICollection<Node> nodes, ICollection<Edge> edges)
        {
            this.nodes = nodes;
            this.edges = edges;
            IsSplitted = true;
        }


        private Bitmap DoLabels()
        {
            if (generatedImage == null)
                throw new Exception("Generate bitmap first");
            labeledImage = new Bitmap(generatedImage.Width, generatedImage.Height);
            Brush labelBrush = new SolidBrush(SettingsKeeper.Data.LabelColor);
            using (Graphics g = Graphics.FromImage(labeledImage))
            {
                g.SmoothingMode = SettingsKeeper.Data.SmoothingMode;
                g.DrawImage(generatedImage, new Point(0,0));
                Font temp = new Font(SettingsKeeper.Data.FontFamily, SettingsKeeper.Data.FontSize, SettingsKeeper.Data.FontStyle);

                foreach (var node in nodes)
                {
                    g.DrawString(node.NodeLabel, temp, labelBrush, node.X + loopSpacing + NodeRadius/2, node.Y + loopSpacing + NodeRadius/2);
                }
            }
            
            return labeledImage;
        }
        /// <summary>
        /// Groups the edges.
        /// </summary>
        /// <returns>Set of list a similar edges</returns>
        private ISet<IList<Edge>> GroupEdges()
        {
            ISet<IList<Edge>> test = new HashSet<IList<Edge>>();
            bool eq = false;
            foreach (var item in edges)
            {
                foreach (var sorted in test)
                {
                    if (sorted.First().Equals(item))
                    {
                        sorted.Add(item);
                        eq = true;
                    }   
                }
                if(eq==false && !item.IsLoopback)
                    test.Add(new List<Edge>() { item });
                eq = false;
            }
            return test;
        }
    }
}

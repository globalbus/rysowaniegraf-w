﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RysowanieGrafów
{
    /// <summary>
    /// Small helper class to modify default FontFamily ToString() method
    /// </summary>
    public class FontFamilyMod
    {
        private FontFamily item;

        public FontFamily Item
        {
            get { return item; }
        }
        public FontFamilyMod(FontFamily item)
        {
            this.item = item;
        }
        public override string ToString()
        {
            return item.Name;
        }
    }
}

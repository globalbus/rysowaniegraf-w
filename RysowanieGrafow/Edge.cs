﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RysowanieGrafów
{
    public class Edge :IEquatable<Edge> 
    {
        bool isBidirectional;//if edge is bidirectional, order in field "nodes" is irrelevant
        bool isLoopback;
        private Node node1;
        private Node node2;
        public Node Node1 { get { return node1; } }
        public Node Node2 { get { return node2; } }
        public bool IsBidirectional { get { return isBidirectional; } }
        public bool IsLoopback { get { return isLoopback; } }
        /// <summary>
        /// Initializes a new instance of the <see cref="Edge"/> class.
        /// </summary>
        /// <param name="node1">The node1.</param>
        /// <param name="node2">The node2.</param>
        /// <param name="isBidirectional">if set to <c>true</c> [is bidirectional].</param>
        public Edge(Node node1, Node node2, bool isBidirectional)
        {
                
            this.node1 = node1;
            this.node2 = node2;
            if (node1 == node2)//loopback is always bidirectional
            {
                this.isLoopback = true;
                this.isBidirectional = true;
            }
            else
                this.isBidirectional = isBidirectional;
            
        }
        /// <summary>
        /// Human readable form of a Edge
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return String.Format("{0} to {1} with bidirectional - {2}", node1.NodeId, node2.NodeId, isBidirectional);
        }

        public bool Equals(Edge other)
        {
            return (this.Node1.Equals(other.Node1) && this.Node2.Equals(other.Node2) || this.Node1.Equals(other.Node2) && this.Node2.Equals(other.Node1));
        }
    }
}

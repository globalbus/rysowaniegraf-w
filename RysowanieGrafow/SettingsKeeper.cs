﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using RysowanieGrafów.Properties;
using System.Drawing.Drawing2D;

namespace RysowanieGrafów
{
    /// <summary>
    /// Specific para-singleton class to keep global drawing settings
    /// XML Serializable
    /// </summary>
    public class SettingsKeeper
    {
        #region Properties
        private SmoothingMode smoothingMode;
        /// <summary>
        /// Gets or sets the GDI smoothing mode.
        /// </summary>
        /// <value>
        /// The smoothing mode.
        /// </value>
        public SmoothingMode SmoothingMode
        {
            get { return smoothingMode; }
            set { smoothingMode = value; }
        }
        
        private int nodeRadius;
        /// <summary>
        /// Gets or sets the node radius.
        /// </summary>
        /// <value>
        /// The node radius.
        /// </value>
        public int NodeRadius
        {
            get { return nodeRadius; }
            set { nodeRadius = value; }
        }
        private int edgeThickness;
        /// <summary>
        /// Gets or sets the edge thickness.
        /// </summary>
        /// <value>
        /// The edge thickness.
        /// </value>
        public int EdgeThickness
        {
            get { return edgeThickness; }
            set { edgeThickness = value; }
        }

        private Color nodeColor;
        /// <summary>
        /// Gets or sets the color of the node.
        /// </summary>
        /// <value>
        /// The color of the node.
        /// </value>
        [XmlIgnore]
        public Color NodeColor
        {
            get { return nodeColor; }
            set { nodeColor = value; }
        }
        /// <summary>
        /// Gets or sets the ARGB of node color.
        /// Use only for XML Serialization
        /// </summary>
        /// <value>
        /// The ARGB of the node color.
        /// </value>
        public int CodedNodeColor
        {
            get { return nodeColor.ToArgb(); }
            set { nodeColor = Color.FromArgb(value); }
        }
        private Color edgeColor;
        /// <summary>
        /// Gets or sets the color of the edge.
        /// </summary>
        /// <value>
        /// The color of the edge.
        /// </value>
        [XmlIgnore]
        public Color EdgeColor
        {
            get { return edgeColor; }
            set { edgeColor = value; }
        }
        /// <summary>
        /// Gets or sets the ARGB of edge color.
        /// Use only for XML Serialization
        /// </summary>
        /// <value>
        /// The ARGB of the edge color.
        /// </value>
        public int CodedEdgeColor
        {
            get { return edgeColor.ToArgb(); }
            set { edgeColor = Color.FromArgb(value); }
        }
        private Color labelColor;
        /// <summary>
        /// Gets or sets the color of the labels.
        /// </summary>
        /// <value>
        /// The color of the labels.
        /// </value>
        [XmlIgnore]
        public Color LabelColor
        {
            get { return labelColor; }
            set { labelColor = value; }
        }
        /// <summary>
        /// Gets or sets the ARGB of labels color.
        /// Use only for XML Serialization
        /// </summary>
        /// <value>
        /// The ARGB of the labels color.
        /// </value>
        public int CodedLabelColor
        {
            get { return labelColor.ToArgb(); }
            set { labelColor = Color.FromArgb(value); }
        }
        private FontFamily fontFamily;
        [XmlIgnore]
        public FontFamily FontFamily
        {
            get { return fontFamily; }
            set { fontFamily = value; }
        }
        public String CodedFontFamily
        {
            get { return fontFamily.Name; }
            set { fontFamily = new FontFamily(value); }
        }
        private FontStyle fontStyle;

        public FontStyle FontStyle
        {
            get { return fontStyle; }
            set { fontStyle = value; }
        }
        
        private float fontSize;

        public float FontSize
        {
            get { return fontSize; }
            set { fontSize = value; }
        }
        private bool isLabeled;

        public bool IsLabeled
        {
            get { return isLabeled; }
            set { isLabeled = value; }
        }
        
        
        #endregion

        /// <summary>
        /// Singleton placement
        /// </summary>
        static SettingsKeeper instance;
        /// <summary>
        /// Internal use serializer
        /// </summary>
        static XmlSerializer serializer;
        /// <summary>
        /// Occurs when XML Serialization Failed
        /// </summary>
        public static event EventHandler FileSystemProblem;
        /// <summary>
        /// Fires the file system error event
        /// </summary>
        private static void FireFileSystemErrorEvent()
        {
            if (FileSystemProblem != null)
                FileSystemProblem.Invoke(null, EventArgs.Empty);
        }
        /// <summary>
        /// Initializes the singleton instance of a class.
        /// </summary>
        static SettingsKeeper()
        {
            serializer = new XmlSerializer(typeof(SettingsKeeper));
            try
            {
                instance = LoadSettings();
            }
            catch (Exception)
            {
                instance = new SettingsKeeper(true);
                FireFileSystemErrorEvent();
            }
        }

        /// <summary>
        /// Place for setting default values, called then XML Serialization failed
        /// </summary>
        private SettingsKeeper()
        {
        }
        private SettingsKeeper(bool defaults)
        {
            edgeThickness = 5;
            nodeRadius = 50;
            nodeColor = Color.Blue;
            edgeColor = Color.Black;
            smoothingMode = SmoothingMode.HighQuality;
            labelColor = Color.Black;
            isLabeled = true;
            fontFamily = new System.Drawing.FontFamily("Arial");
            fontSize = 12.0f;
            fontStyle = System.Drawing.FontStyle.Regular;
        }

        /// <summary>
        /// Gets or sets the singleton instance
        /// </summary>
        /// <value>
        /// Instance of a class
        /// </value>
        public static SettingsKeeper Data
        {
            get
            {
                return instance;
            }
            set
            {
                instance = value;
                try
                {
                    SaveSettings();
                }
                catch (Exception)
                {
                    FireFileSystemErrorEvent();
                }
            }
        }
        /// <summary>
        /// Gets the new non-singleton instance for fail-save changing settings
        /// </summary>
        /// <returns>non-singleton instance of a class</returns>
        public static SettingsKeeper GetNewInstance()
        {
            SettingsKeeper temp = null;
            try
            {
                temp = LoadSettings();
            }
            catch (Exception)
            {
                temp = new SettingsKeeper(true);
                FireFileSystemErrorEvent();
            }
            return temp;
        }
        #region Serialization
        /// <summary>
        /// Loads the settings via XML Serialization
        /// </summary>
        /// <returns></returns>
        private static SettingsKeeper LoadSettings()
        {
            SettingsKeeper temp;
            using (var fs = new FileStream(Resources.DefaultSettingsFile, FileMode.Open))
            {
                temp = (SettingsKeeper)serializer.Deserialize(fs);
            }
            return temp;
        }
        /// <summary>
        /// Saves the settings via XML Serialization
        /// </summary>
        private static void SaveSettings()
        {
            using (var fs = new FileStream(Resources.DefaultSettingsFile, FileMode.Create))
            {
                serializer.Serialize(fs, instance);
            }
        }
        #endregion
    }
}

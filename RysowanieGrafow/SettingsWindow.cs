﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace RysowanieGrafów
{
    public partial class SettingsWindow : Form
    {
        private SettingsKeeper tempSettingsKeeper;
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsWindow"/> class.
        /// Read values from <see cref="SettingsKeeper"/> class and set it to Form controls attributes
        /// </summary>
        public SettingsWindow()
        {
            InitializeComponent();
            tempSettingsKeeper = SettingsKeeper.GetNewInstance();
            this.nodeColor.BackColor = tempSettingsKeeper.NodeColor;
            this.nodeSizeSelector.Value = tempSettingsKeeper.NodeRadius;
            this.edgeThicknessSelector.Value = tempSettingsKeeper.EdgeThickness;
            this.edgeColor.BackColor = tempSettingsKeeper.EdgeColor;
            this.smoothingModeList.DataSource = Enum.GetNames(typeof(SmoothingMode)).Take(5).ToList();
            this.smoothingModeList.SelectedItem = Enum.GetName(typeof(SmoothingMode), SettingsKeeper.Data.SmoothingMode);
            this.isLabeledCheckbox.Checked = tempSettingsKeeper.IsLabeled;
            this.labeledPanel.Enabled = tempSettingsKeeper.IsLabeled;
            List<FontFamilyMod> fontlist = new List<FontFamilyMod>();
            foreach (var item in FontFamily.Families)
	        {
		        fontlist.Add(new FontFamilyMod(item));
	        }
            this.fontFamilyCombobox.DataSource = fontlist;
            this.fontFamilyCombobox.SelectedItem= fontlist.FirstOrDefault(x => x.Item.Equals(tempSettingsKeeper.FontFamily));
            this.fontSizeSelector.Value = (decimal)tempSettingsKeeper.FontSize;
            this.fontColor.BackColor = tempSettingsKeeper.LabelColor;
            this.fontStyleCombobox.DataSource = Enum.GetNames(typeof(FontStyle));
            this.fontStyleCombobox.SelectedItem = Enum.GetName(typeof(FontStyle), SettingsKeeper.Data.FontStyle);
            this.fontFamilyCombobox.SelectedIndexChanged += new System.EventHandler(this.fontFamilyCombobox_SelectedIndexChanged);
        }

        private void nodeColor_Click(object sender, EventArgs e)
        {
            colorSelector.Color = tempSettingsKeeper.NodeColor;
            colorSelector.ShowDialog();
            tempSettingsKeeper.NodeColor=colorSelector.Color;
            nodeColor.BackColor = colorSelector.Color;
        }

        private void nodeSizeSelector_ValueChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.NodeRadius = (int)nodeSizeSelector.Value;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            SettingsKeeper.Data = tempSettingsKeeper;
            this.Hide();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void edgeColor_Click(object sender, EventArgs e)
        {
            colorSelector.Color = tempSettingsKeeper.EdgeColor;
            colorSelector.ShowDialog();
            tempSettingsKeeper.EdgeColor = colorSelector.Color;
            edgeColor.BackColor = colorSelector.Color;
        }

        private void edgeThicknessSelector_ValueChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.EdgeThickness = (int)edgeThicknessSelector.Value;
        }

        private void SmoothingModeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.SmoothingMode =(SmoothingMode) Enum.ToObject(typeof(SmoothingMode), smoothingModeList.SelectedIndex);
        }

        private void isLabeledCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            labeledPanel.Enabled = isLabeledCheckbox.Checked;
            tempSettingsKeeper.IsLabeled = isLabeledCheckbox.Checked;
        }

        private void fontFamilyCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.FontFamily = ((FontFamilyMod)fontFamilyCombobox.SelectedItem).Item;
        }

        private void fontSizeSelector_ValueChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.FontSize = (float)fontSizeSelector.Value;
        }

        private void fontColor_Click(object sender, EventArgs e)
        {
            colorSelector.Color = tempSettingsKeeper.LabelColor;
            colorSelector.ShowDialog();
            tempSettingsKeeper.LabelColor = colorSelector.Color;
            fontColor.BackColor = colorSelector.Color;
        }

        private void fontStyleCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            tempSettingsKeeper.FontStyle = (FontStyle)Enum.ToObject(typeof(FontStyle), fontStyleCombobox.SelectedIndex);
        }
    }
}

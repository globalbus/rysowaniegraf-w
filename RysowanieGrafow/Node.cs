﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RysowanieGrafów
{
    public class Node : IEquatable<Node> 
    {
        ICollection<Node> neighbors;
        ICollection<Edge> edgesRelatedWithThisNode;
        private int nodeId;
        private string nodeLabel;

        public string NodeLabel
        {
            get { return nodeLabel; }
        }
        public int NodeId { get { return nodeId; } }
        public int Degree { get { return neighbors.Count; } }

        private int x;
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        private double angle;

        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }
        
        public Node(int nodeId, string nodeLabel)
        {
            this.nodeId = nodeId;
            this.nodeLabel = nodeLabel;
            edgesRelatedWithThisNode = new List<Edge>();
            neighbors = new List<Node>();
        }
        public ICollection<Edge> EdgesRelatedWithThisNode { get { return (edgesRelatedWithThisNode as List<Edge>).AsReadOnly(); } }

        /// <summary>
        /// Adds the edge related with this node and adds neighbor if possible.
        /// Nieghbor should be a node with direct connection to this node.
        /// </summary>
        /// <param name="edge">The edge.</param>
        internal void AddEdge(Edge edge)
        {
            edgesRelatedWithThisNode.Add(edge);
            if (edge.Node1.NodeId != this.nodeId && edge.IsBidirectional && !neighbors.Contains(edge.Node1))
                neighbors.Add(edge.Node1);
            else if (edge.Node2.NodeId != this.nodeId && !neighbors.Contains(edge.Node2))
                neighbors.Add(edge.Node2);
        }
        /// <summary>
        /// Human readable form of a Node
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string neighborsString=null;
            foreach (var item in neighbors)
            {
                neighborsString += item.NodeId + ", ";
            }
            return String.Format("Node {0} with neighbors  - {1}", nodeId, neighborsString);
        }

        public bool Equals(Node other)
        {
            return other.NodeId == this.NodeId;
        }
    }
}

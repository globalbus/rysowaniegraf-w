﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RysowanieGrafów
{
    internal class EqComparer : IEqualityComparer<Edge>
    {
        public bool Equals(Edge x, Edge y)
        {
            return ((Object)x).Equals((Object)y);
        }

        public int GetHashCode(Edge obj)
        {
            return obj.GetHashCode();
        }
    }
}

﻿namespace RysowanieGrafów
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsWindow));
            this.CancelButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.nodeSizeSelector = new System.Windows.Forms.NumericUpDown();
            this.nodeSizeLabel = new System.Windows.Forms.Label();
            this.nodeColor = new System.Windows.Forms.Button();
            this.colorSelector = new System.Windows.Forms.ColorDialog();
            this.edgeColor = new System.Windows.Forms.Button();
            this.edgeThicknessSelector = new System.Windows.Forms.NumericUpDown();
            this.edgeThicknessLabel = new System.Windows.Forms.Label();
            this.nodeColorLabel = new System.Windows.Forms.Label();
            this.edgeColorLabel = new System.Windows.Forms.Label();
            this.smoothingModeList = new System.Windows.Forms.ComboBox();
            this.smoothingModeLabel = new System.Windows.Forms.Label();
            this.isLabeledCheckbox = new System.Windows.Forms.CheckBox();
            this.labeledPanel = new System.Windows.Forms.Panel();
            this.fontFamilyCombobox = new System.Windows.Forms.ComboBox();
            this.fontFamilyLabel = new System.Windows.Forms.Label();
            this.fontSizeLabel = new System.Windows.Forms.Label();
            this.fontSizeSelector = new System.Windows.Forms.NumericUpDown();
            this.fontColorLabel = new System.Windows.Forms.Label();
            this.fontColor = new System.Windows.Forms.Button();
            this.fontStyleLabel = new System.Windows.Forms.Label();
            this.fontStyleCombobox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nodeSizeSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeThicknessSelector)).BeginInit();
            this.labeledPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fontSizeSelector)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(194, 278);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(85, 28);
            this.CancelButton.TabIndex = 0;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(12, 278);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(94, 28);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "Apply";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // nodeSizeSelector
            // 
            this.nodeSizeSelector.Location = new System.Drawing.Point(12, 39);
            this.nodeSizeSelector.Name = "nodeSizeSelector";
            this.nodeSizeSelector.Size = new System.Drawing.Size(120, 20);
            this.nodeSizeSelector.TabIndex = 2;
            this.nodeSizeSelector.ValueChanged += new System.EventHandler(this.nodeSizeSelector_ValueChanged);
            // 
            // nodeSizeLabel
            // 
            this.nodeSizeLabel.AutoSize = true;
            this.nodeSizeLabel.Location = new System.Drawing.Point(12, 23);
            this.nodeSizeLabel.Name = "nodeSizeLabel";
            this.nodeSizeLabel.Size = new System.Drawing.Size(56, 13);
            this.nodeSizeLabel.TabIndex = 3;
            this.nodeSizeLabel.Text = "Node Size";
            // 
            // nodeColor
            // 
            this.nodeColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nodeColor.Location = new System.Drawing.Point(160, 39);
            this.nodeColor.Name = "nodeColor";
            this.nodeColor.Size = new System.Drawing.Size(32, 20);
            this.nodeColor.TabIndex = 4;
            this.nodeColor.UseVisualStyleBackColor = true;
            this.nodeColor.Click += new System.EventHandler(this.nodeColor_Click);
            // 
            // colorSelector
            // 
            this.colorSelector.AnyColor = true;
            this.colorSelector.SolidColorOnly = true;
            // 
            // edgeColor
            // 
            this.edgeColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.edgeColor.Location = new System.Drawing.Point(160, 83);
            this.edgeColor.Name = "edgeColor";
            this.edgeColor.Size = new System.Drawing.Size(32, 20);
            this.edgeColor.TabIndex = 5;
            this.edgeColor.UseVisualStyleBackColor = true;
            this.edgeColor.Click += new System.EventHandler(this.edgeColor_Click);
            // 
            // edgeThicknessSelector
            // 
            this.edgeThicknessSelector.Location = new System.Drawing.Point(12, 83);
            this.edgeThicknessSelector.Name = "edgeThicknessSelector";
            this.edgeThicknessSelector.Size = new System.Drawing.Size(120, 20);
            this.edgeThicknessSelector.TabIndex = 6;
            this.edgeThicknessSelector.ValueChanged += new System.EventHandler(this.edgeThicknessSelector_ValueChanged);
            // 
            // edgeThicknessLabel
            // 
            this.edgeThicknessLabel.AutoSize = true;
            this.edgeThicknessLabel.Location = new System.Drawing.Point(12, 67);
            this.edgeThicknessLabel.Name = "edgeThicknessLabel";
            this.edgeThicknessLabel.Size = new System.Drawing.Size(84, 13);
            this.edgeThicknessLabel.TabIndex = 7;
            this.edgeThicknessLabel.Text = "Edge Thickness";
            // 
            // nodeColorLabel
            // 
            this.nodeColorLabel.AutoSize = true;
            this.nodeColorLabel.Location = new System.Drawing.Point(157, 23);
            this.nodeColorLabel.Name = "nodeColorLabel";
            this.nodeColorLabel.Size = new System.Drawing.Size(60, 13);
            this.nodeColorLabel.TabIndex = 8;
            this.nodeColorLabel.Text = "Node Color";
            // 
            // edgeColorLabel
            // 
            this.edgeColorLabel.AutoSize = true;
            this.edgeColorLabel.Location = new System.Drawing.Point(157, 67);
            this.edgeColorLabel.Name = "edgeColorLabel";
            this.edgeColorLabel.Size = new System.Drawing.Size(59, 13);
            this.edgeColorLabel.TabIndex = 9;
            this.edgeColorLabel.Text = "Edge Color";
            // 
            // smoothingModeList
            // 
            this.smoothingModeList.FormattingEnabled = true;
            this.smoothingModeList.Location = new System.Drawing.Point(12, 125);
            this.smoothingModeList.Name = "smoothingModeList";
            this.smoothingModeList.Size = new System.Drawing.Size(120, 21);
            this.smoothingModeList.TabIndex = 11;
            this.smoothingModeList.SelectedIndexChanged += new System.EventHandler(this.SmoothingModeList_SelectedIndexChanged);
            // 
            // smoothingModeLabel
            // 
            this.smoothingModeLabel.AutoSize = true;
            this.smoothingModeLabel.Location = new System.Drawing.Point(9, 109);
            this.smoothingModeLabel.Name = "smoothingModeLabel";
            this.smoothingModeLabel.Size = new System.Drawing.Size(109, 13);
            this.smoothingModeLabel.TabIndex = 12;
            this.smoothingModeLabel.Text = "GDI Smoothing Mode";
            // 
            // isLabeledCheckbox
            // 
            this.isLabeledCheckbox.AutoSize = true;
            this.isLabeledCheckbox.Location = new System.Drawing.Point(12, 152);
            this.isLabeledCheckbox.Name = "isLabeledCheckbox";
            this.isLabeledCheckbox.Size = new System.Drawing.Size(64, 17);
            this.isLabeledCheckbox.TabIndex = 13;
            this.isLabeledCheckbox.TabStop = false;
            this.isLabeledCheckbox.Text = "Labeled";
            this.isLabeledCheckbox.UseVisualStyleBackColor = true;
            this.isLabeledCheckbox.CheckedChanged += new System.EventHandler(this.isLabeledCheckbox_CheckedChanged);
            // 
            // labeledPanel
            // 
            this.labeledPanel.Controls.Add(this.fontStyleCombobox);
            this.labeledPanel.Controls.Add(this.fontStyleLabel);
            this.labeledPanel.Controls.Add(this.fontColor);
            this.labeledPanel.Controls.Add(this.fontColorLabel);
            this.labeledPanel.Controls.Add(this.fontSizeSelector);
            this.labeledPanel.Controls.Add(this.fontSizeLabel);
            this.labeledPanel.Controls.Add(this.fontFamilyLabel);
            this.labeledPanel.Controls.Add(this.fontFamilyCombobox);
            this.labeledPanel.Location = new System.Drawing.Point(12, 169);
            this.labeledPanel.Name = "labeledPanel";
            this.labeledPanel.Size = new System.Drawing.Size(267, 103);
            this.labeledPanel.TabIndex = 14;
            // 
            // fontFamilyCombobox
            // 
            this.fontFamilyCombobox.FormattingEnabled = true;
            this.fontFamilyCombobox.Location = new System.Drawing.Point(0, 17);
            this.fontFamilyCombobox.Name = "fontFamilyCombobox";
            this.fontFamilyCombobox.Size = new System.Drawing.Size(120, 21);
            this.fontFamilyCombobox.TabIndex = 12;

            // 
            // fontFamilyLabel
            // 
            this.fontFamilyLabel.AutoSize = true;
            this.fontFamilyLabel.Location = new System.Drawing.Point(0, 3);
            this.fontFamilyLabel.Name = "fontFamilyLabel";
            this.fontFamilyLabel.Size = new System.Drawing.Size(60, 13);
            this.fontFamilyLabel.TabIndex = 13;
            this.fontFamilyLabel.Text = "Font Family";
            // 
            // fontSizeLabel
            // 
            this.fontSizeLabel.AutoSize = true;
            this.fontSizeLabel.Location = new System.Drawing.Point(0, 41);
            this.fontSizeLabel.Name = "fontSizeLabel";
            this.fontSizeLabel.Size = new System.Drawing.Size(51, 13);
            this.fontSizeLabel.TabIndex = 14;
            this.fontSizeLabel.Text = "Font Size";
            // 
            // fontSizeSelector
            // 
            this.fontSizeSelector.Location = new System.Drawing.Point(0, 57);
            this.fontSizeSelector.Name = "fontSizeSelector";
            this.fontSizeSelector.Size = new System.Drawing.Size(120, 20);
            this.fontSizeSelector.TabIndex = 15;
            this.fontSizeSelector.ValueChanged += new System.EventHandler(this.fontSizeSelector_ValueChanged);
            // 
            // fontColorLabel
            // 
            this.fontColorLabel.AutoSize = true;
            this.fontColorLabel.Location = new System.Drawing.Point(145, 3);
            this.fontColorLabel.Name = "fontColorLabel";
            this.fontColorLabel.Size = new System.Drawing.Size(55, 13);
            this.fontColorLabel.TabIndex = 15;
            this.fontColorLabel.Text = "Font Color";
            // 
            // fontColor
            // 
            this.fontColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fontColor.Location = new System.Drawing.Point(148, 19);
            this.fontColor.Name = "fontColor";
            this.fontColor.Size = new System.Drawing.Size(32, 20);
            this.fontColor.TabIndex = 16;
            this.fontColor.UseVisualStyleBackColor = true;
            this.fontColor.Click += new System.EventHandler(this.fontColor_Click);
            // 
            // fontStyleLabel
            // 
            this.fontStyleLabel.AutoSize = true;
            this.fontStyleLabel.Location = new System.Drawing.Point(145, 42);
            this.fontStyleLabel.Name = "fontStyleLabel";
            this.fontStyleLabel.Size = new System.Drawing.Size(54, 13);
            this.fontStyleLabel.TabIndex = 17;
            this.fontStyleLabel.Text = "Font Style";
            // 
            // fontStyleCombobox
            // 
            this.fontStyleCombobox.FormattingEnabled = true;
            this.fontStyleCombobox.Location = new System.Drawing.Point(144, 57);
            this.fontStyleCombobox.Name = "fontStyleCombobox";
            this.fontStyleCombobox.Size = new System.Drawing.Size(120, 21);
            this.fontStyleCombobox.TabIndex = 18;
            this.fontStyleCombobox.SelectedIndexChanged += new System.EventHandler(this.fontStyleCombobox_SelectedIndexChanged);
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 318);
            this.Controls.Add(this.labeledPanel);
            this.Controls.Add(this.isLabeledCheckbox);
            this.Controls.Add(this.smoothingModeLabel);
            this.Controls.Add(this.smoothingModeList);
            this.Controls.Add(this.edgeColorLabel);
            this.Controls.Add(this.nodeColorLabel);
            this.Controls.Add(this.edgeThicknessLabel);
            this.Controls.Add(this.edgeThicknessSelector);
            this.Controls.Add(this.edgeColor);
            this.Controls.Add(this.nodeColor);
            this.Controls.Add(this.nodeSizeLabel);
            this.Controls.Add(this.nodeSizeSelector);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CancelButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsWindow";
            this.ShowInTaskbar = false;
            this.Text = "SettingsWindow";
            ((System.ComponentModel.ISupportInitialize)(this.nodeSizeSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeThicknessSelector)).EndInit();
            this.labeledPanel.ResumeLayout(false);
            this.labeledPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fontSizeSelector)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.NumericUpDown nodeSizeSelector;
        private System.Windows.Forms.Label nodeSizeLabel;
        private System.Windows.Forms.Button nodeColor;
        private System.Windows.Forms.ColorDialog colorSelector;
        private System.Windows.Forms.Button edgeColor;
        private System.Windows.Forms.NumericUpDown edgeThicknessSelector;
        private System.Windows.Forms.Label edgeThicknessLabel;
        private System.Windows.Forms.Label nodeColorLabel;
        private System.Windows.Forms.Label edgeColorLabel;
        private System.Windows.Forms.ComboBox smoothingModeList;
        private System.Windows.Forms.Label smoothingModeLabel;
        private System.Windows.Forms.CheckBox isLabeledCheckbox;
        private System.Windows.Forms.Panel labeledPanel;
        private System.Windows.Forms.Label fontFamilyLabel;
        private System.Windows.Forms.ComboBox fontFamilyCombobox;
        private System.Windows.Forms.NumericUpDown fontSizeSelector;
        private System.Windows.Forms.Label fontSizeLabel;
        private System.Windows.Forms.Label fontColorLabel;
        private System.Windows.Forms.ComboBox fontStyleCombobox;
        private System.Windows.Forms.Label fontStyleLabel;
        private System.Windows.Forms.Button fontColor;
    }
}
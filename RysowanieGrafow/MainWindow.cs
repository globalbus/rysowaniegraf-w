﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace RysowanieGrafów
{
    public partial class MainWindow : Form
    {
        private Graph graph;
        private ICollection<Graph> graphSet;
        private Form settingsWindow;
        public MainWindow()
        {
            InitializeComponent();
            SettingsKeeper.FileSystemProblem += new EventHandler(OnSettingsKeeper_FileSystemProblem);
        }

        void OnSettingsKeeper_FileSystemProblem(object sender, EventArgs e)
        {
            MessageBox.Show("Problem while reading/writing configuration file");
        }
        private void readToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                readGphFile.ShowDialog();
                graph = new Graph(readGphFile.FileName);
                redrawToolStripMenuItem.Enabled = true;
                redrawToolStripMenuItem_Click(this, EventArgs.Empty);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(settingsWindow==null||settingsWindow.IsDisposed)
                settingsWindow = new SettingsWindow();
            settingsWindow.Show();
            settingsWindow.Focus();
        }

        private void redrawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (graph != null)
            {
                graphSet = graph.Split();
                graphPlot.Image = Graph.ToBitmap(graphSet);
                if (graphPlot.Image != null)
                {
                    saveResultToolStripMenuItem.Enabled = true;
                    graphPlot.Visible = true;
                }
                else
                {
                    saveResultToolStripMenuItem.Enabled = false;
                    graphPlot.Visible = false;
                }
            }
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
                redrawToolStripMenuItem_Click(this, EventArgs.Empty);
        }

        private void saveResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
                graphPlot.Image.Save(saveFileDialog1.FileName, ImageFormat.Png);
        }

    }
}

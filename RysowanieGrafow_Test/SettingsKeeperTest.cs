﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RysowanieGrafów;

namespace RysowanieGrafow_Test
{
    [TestClass]
    public class SettingsKeeperTest
    {
        [TestMethod]
        public void InstanceTest()
        {
            int counter = 0;
            EventHandler eventHandler = new EventHandler((o, e) => { counter++;});
            SettingsKeeper instance = SettingsKeeper.Data;
            SettingsKeeper.FileSystemProblem += eventHandler;
            SettingsKeeper newinstance = SettingsKeeper.GetNewInstance();
            Assert.IsNotNull(instance);
            Assert.AreNotSame(instance, newinstance);
            SettingsKeeper.Data = newinstance;
            Assert.AreSame(SettingsKeeper.Data, newinstance);
            Assert.AreEqual(counter, 1);
        }
    }
}

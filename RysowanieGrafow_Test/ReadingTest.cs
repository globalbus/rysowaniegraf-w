﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RysowanieGrafów;
using RysowanieGrafow_Test.Properties;
using System.Drawing;
using System.Drawing.Imaging;

namespace RysowanieGrafow_Test
{
    [TestClass]
    public class ReadingTest
    {
        public ReadingTest()
        {
        }

        [TestMethod]
        public void ReadEmptyGraph()
        {
            Object file = Resources.SampleEmpty;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 0);
            Assert.AreEqual(test.Nodes.Count, 0);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 0);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            Assert.IsNull(visualInspection);
        }
        [TestMethod]
        public void ReadK1Graph()
        {
            Object file = Resources.SampleK1;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 0);
            Assert.AreEqual(test.Nodes.Count, 1);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 1);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("K1.png", ImageFormat.Png);
        }
        [TestMethod]
        public void ReadK5Graph()
        {
            Object file = Resources.SampleK5;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 4 + 3 + 2 + 1);
            Assert.AreEqual(test.Nodes.Count, 5);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 1);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("K5.png", ImageFormat.Png);
        }
        [TestMethod]
        public void ReadK5WithLoopsGraph()
        {
            Object file = Resources.SampleK5withLoops;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 4 + 3 + 2 + 1 + 5);
            Assert.AreEqual(test.Nodes.Count, 5);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 1);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("K5withLoops.png", ImageFormat.Png);
        }
        [TestMethod]
        public void ReadK5WithMulti()
        {
            Object file = Resources.SampleK5withMulti;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 4 + 3 + 2 + 1 + 5 + 2);
            Assert.AreEqual(test.Nodes.Count, 5);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 1);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("K5withMulti.png", ImageFormat.Png);
        }
        [TestMethod]
        public void ReadMultiEdgeGraph()
        {
            Object file = Resources.SampleMultiEdgeTest;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Nodes.Count, 2);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreEqual(graphSet.Count, 1);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("MultiEdge.png", ImageFormat.Png);
        }
        [TestMethod]
        public void SplittingGraph()
        {
            Object file = Resources.SampleSplitting;
            Graph test = new Graph((byte[])file);
            ICollection<Graph> graphSet = test.Split();
            Assert.AreNotEqual(graphSet.Count, 1);
            int edges = 0, nodes = 0;
            foreach (var item in graphSet)
            {
                edges += item.Edges.Count;
                nodes += item.Nodes.Count;
            }
            Assert.AreEqual(test.Nodes.Count, edges);
            Assert.AreEqual(test.Edges.Count, nodes);
            Bitmap visualInspection = Graph.ToBitmap(graphSet);
            visualInspection.Save("MultiEdge.png", ImageFormat.Png);
        }
        [TestMethod]
        public void InvalidEmptyFile()
        {
            Object file = Resources.InvalidEmpty;
            Graph test = new Graph((byte[])file);
            Assert.AreEqual(test.Edges.Count, 0);
            Assert.AreEqual(test.Nodes.Count, 0);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void InvalidNodeNotExists()
        {
            Object file = Resources.InvalidNodeNotExists;
            Graph test = new Graph((byte[])file);
        }
        [TestMethod]
        public void InvalidNoLabel()
        {
            Object file = Resources.InvalidNoLabel;
            Graph test = new Graph((byte[])file);
        }
        [TestMethod]
        public void InvalidUnknownRelation()
        {
            Object file = Resources.InvalidUnknownRelation;
            Graph test = new Graph((byte[])file);
        }
    }
}
